﻿using DayOfTheWeek.Service;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzzModelTest
{
    public class IsDayOfWeekMatchesTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestIsDayOfWeekMatches_WhenDayIsMatching()
        {
            List<string> expectedDay = new List<string> { "MONDAY","TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY" };
            var objResult = new DayOfTheWeekCase();
            var actualDay = objResult.CurrentDayOfTheWeek();

            Assert.That(expectedDay.Count(p => p.Contains(actualDay.ToUpper())), Is.EqualTo(1));
        }
    }
}
