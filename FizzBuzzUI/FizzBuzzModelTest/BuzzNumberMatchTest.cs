using FizzBuzzRuleAll;
using NUnit.Framework;
using System.Collections.Generic;

namespace FizzBuzzModelTest
{
    public class BuzzNumberMatchTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestBuzzRule_NumberCheck_DivisibleBy5_Pass()
        {
            var objResult = new BuzzRule();
            bool actualResult = objResult.NumberMatch(20);

            Assert.Equals(true, actualResult);
        }

        [Test]
        public void TestBuzzRule_NumberCheck_DivisibleBy5_Fail()
        {
            var objResult = new BuzzRule();
            bool actualResult = objResult.NumberMatch(7);

            Assert.Equals(false, actualResult);
        }
    }
}