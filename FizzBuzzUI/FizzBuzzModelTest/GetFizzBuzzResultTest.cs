using FizzBuzzResult;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace FizzBuzzModelTest
{
    public class GetFizzBuzzResultTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGenerateFizzBuzz()
        {
            var expectedResult = new List<string> { "1", "2", "Fizz", "4", "Buzz" };

            var objResult = new GetFizzBuzzResult();
            var actualResult = objResult.GenerateFizzBuzz(5);

            CollectionAssert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void TestProcessFizzBuzzData_ReturnInteger()
        {
            var expectedResult = "7";

            var objResult = new GetFizzBuzzResult();
            MethodInfo methodInfo = typeof(GetFizzBuzzResult).GetMethod("ProcessFizzBuzzData", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 7 };
            var actualResult = methodInfo.Invoke(objResult, parameters);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void TestProcessFizzBuzzData_ReturnFizz()
        {
            List<string> expectedResult = new List<string> { "FIZZ", "WIZZ" };

            var objResult = new GetFizzBuzzResult();
            MethodInfo methodInfo = typeof(GetFizzBuzzResult).GetMethod("ProcessFizzBuzzData", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 6 };
            var actualResult = methodInfo.Invoke(objResult, parameters);
            Assert.That(expectedResult.Count(p => p.Contains(actualResult.ToString().ToUpper())), Is.EqualTo(1));
        }

        [Test]
        public void TestProcessFizzBuzzData_ReturnBuzz()
        {
            List<string> expectedResult = new List<string> { "BUZZ", "WUZZ" };

            var objResult = new GetFizzBuzzResult();
            MethodInfo methodInfo = typeof(GetFizzBuzzResult).GetMethod("ProcessFizzBuzzData", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 10 };
            var actualResult = methodInfo.Invoke(objResult, parameters);

            Assert.That(expectedResult.Count(p => p.Contains(actualResult.ToString().ToUpper())), Is.EqualTo(1));
        }

        [Test]
        public void TestProcessFizzBuzzData_ReturnFizzBuzz()
        {
            List<string> expectedResult = new List<string> { "FIZZBUZZ", "WIZZWUZZ" };

            var objResult = new GetFizzBuzzResult();
            MethodInfo methodInfo = typeof(GetFizzBuzzResult).GetMethod("ProcessFizzBuzzData", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 30 };
            var actualResult = methodInfo.Invoke(objResult, parameters);

            Assert.That(expectedResult.Count(p => p.Contains(actualResult.ToString().ToUpper())), Is.EqualTo(1));
        }
    }
}