using FizzBuzzRuleAll;
using NUnit.Framework;
using System.Collections.Generic;

namespace FizzBuzzModelTest
{
    public class FizzNumberCheckTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestFizzRule_NumberCheck_DivisibleBy3_Pass()
        {
            var objResult = new FizzRule();
            bool actualResult = objResult.NumberMatch(6);

            Assert.Equals(true, actualResult);
        }

        [Test]
        public void TestFizzRule_NumberCheck_DivisibleBy3_Fail()
        {
            var objResult = new FizzRule();
            bool actualResult = objResult.NumberMatch(5);

            Assert.Equals(false, actualResult);
        }
    }
}