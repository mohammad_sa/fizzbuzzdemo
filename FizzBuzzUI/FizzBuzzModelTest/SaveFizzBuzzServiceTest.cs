﻿using DayOfTheWeek.Service;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SaveFizzBuzzService.Interface;

namespace FizzBuzzModelTest
{
    public class SaveFizzBuzzServiceTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestFizzBuzzEnteredDataSaved()
        {
            var obj = new Mock<ISaveFizzBuzzData>();
            obj.Setup(x => x.SaveDataDB(It.IsAny<int>())).Returns(true);
            var Res = obj.Object.SaveDataDB(10);
            Assert.That(Res, Is.True);
        }
    }
}
