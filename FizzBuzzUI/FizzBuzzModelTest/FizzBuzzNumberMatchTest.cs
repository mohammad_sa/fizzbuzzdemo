using FizzBuzzRuleAll;
using NUnit.Framework;
using System.Collections.Generic;

namespace FizzBuzzModelTest
{
    public class FizzBuzzNumberCheckTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestFizzBuzzRule_NumberCheck_DivisibleBy3And5_Pass()
        {
            var objResult = new FizzRule();
            bool actualResult = objResult.NumberMatch(30);

            Assert.Equals(true, actualResult);
        }

        [Test]
        public void TestFizzBuzzRule_NumberCheck_DivisibleBy3And5_Fail()
        {
            var objResult = new FizzRule();
            bool actualResult = objResult.NumberMatch(10);

            Assert.Equals(false, actualResult);
        }
    }
}