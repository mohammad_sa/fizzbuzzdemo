using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzzUI.Models
{
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
        }
        [Required(ErrorMessage = "Please enter any positive integer number between 1 and 1000")]
        [Range(1, 1000, ErrorMessage ="Input should be positive integer between 1 and 1000")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter any positive integer number between 1 and 1000")]
        public int InputData { get; set; }
    }
}
