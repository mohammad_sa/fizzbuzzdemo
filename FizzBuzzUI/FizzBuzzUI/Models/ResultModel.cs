using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;

namespace FizzBuzzUI.Models
{
    public class ResultModel : PageModel
    {
        public void OnGet()
        {
        }

        public List<string> OutputResult { get; set; }

        public int CurrentPageIndex { get; set; }

        public int PageCount { get; set; }
    }
}
