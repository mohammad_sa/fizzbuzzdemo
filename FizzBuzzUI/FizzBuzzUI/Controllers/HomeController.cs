﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FizzBuzzUI.Models;
using Microsoft.AspNetCore.Http;
using FizzBuzzResult.Interface;
using FizzBuzzRuleAll;
using PagedList;
using SaveFizzBuzzService.Interface;

namespace FizzBuzzUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGetFizzResult _getFizzResult;

        public HomeController(IGetFizzResult getFizzResult)
        {
            _getFizzResult = getFizzResult;
            //_getFizzResult.LoadRules();
        }

        public IActionResult Index()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult ProcessData(IFormCollection form, int currentPageIndex = 1)
        {
            int maxRows = 20;
            int inputData;
            List<string> output = new List<string>();
            if (form["InputData"].Count > 0)
            {
                inputData = Convert.ToInt32(form["InputData"].ToString());
                TempData["inputData"] = inputData;
                //Save entered interger number in DB
                //_iSaveData.SaveDataDB(inputData);
            }
            else
            {
                inputData = Convert.ToInt32(TempData.Peek("inputData"));
            }
            output = _getFizzResult.GenerateFizzBuzz(inputData);

            ViewBag.ProcessedData = output.Skip((currentPageIndex - 1) * maxRows).Take(maxRows);
            double pageCount = (double)(output.Count() / Convert.ToDecimal(maxRows));

            ResultModel processed = new ResultModel
            {
                OutputResult = output,
                CurrentPageIndex = currentPageIndex,
                PageCount = (int)Math.Ceiling(pageCount)
            };

            //IPagedList<ResultModel.> abc = processed.OutputResult.ToPagedList(1,4);

            ViewBag.CurrentPage = processed.CurrentPageIndex;
            ViewBag.PageCount = processed.PageCount;
            return View("Result", processed);
        }
    }
}
