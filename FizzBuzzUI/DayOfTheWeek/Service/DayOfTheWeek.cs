﻿using DayOfTheWeek.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace DayOfTheWeek.Service
{
    public class DayOfTheWeekCase : IDayOfTheWeek
    {
        public string CurrentDayOfTheWeek()
        {
            return DateTime.Today.DayOfWeek.ToString();
        }
    }
}
