﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DayOfTheWeek.Interface
{
    public interface IDayOfTheWeek
    {
        string CurrentDayOfTheWeek();
    }
}
