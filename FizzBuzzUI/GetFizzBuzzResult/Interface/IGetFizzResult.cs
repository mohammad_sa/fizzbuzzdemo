﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzResult.Interface
{
    public interface IGetFizzResult
    {
        public List<string> GenerateFizzBuzz(int enteredNumber);
        //public void LoadRules();
        
    }
}
