﻿using System;
using FizzBuzzResult.Interface;
using System.Collections.Generic;
using FizzBuzzRuleAll;

namespace FizzBuzzResult
{
    public class GetFizzBuzzResult : IGetFizzResult
    {
        private List<IFizzBuzzRule> _iFizzRule;
        //private readonly IFizzBuzzRule _iFizzRule;
        public GetFizzBuzzResult()
        {
            LoadRules();
            //_iFizzRule = iFizzRule;
        }

        //public GetFizzBuzzResult(IFizzBuzzRule iFizzRule)
        //{
        //    //LoadRules();
        //    //_iFizzRule = iFizzRule;
        //}

        public void LoadRules()
        {
            this._iFizzRule = new List<IFizzBuzzRule>
            {
                new FizzRule(),
                new BuzzRule(),
                new FizzRule()
            };
        }
        public List<string> GenerateFizzBuzz(int enteredNumber)
        {
            List<string> output = new List<string>();

            for (int i = 1; i <= enteredNumber; i++)
            {
                output.Add(ProcessFizzBuzzData(i));
            }
            return output;
        }

        private string ProcessFizzBuzzData(int sequenceNumber)
        {
            //bool abc = _iFizzRule.NumberMatch(sequenceNumber);
            foreach (var rule in _iFizzRule)
            {
                if (rule.NumberMatch(sequenceNumber))
                {
                    return rule.GetReplacedWord();
                }
            }
            return Convert.ToString(sequenceNumber);
        }
    }
}
