﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SaveFizzBuzzService.Interface
{
    public interface ISaveFizzBuzzData
    {
        bool SaveDataDB(int enteredData);
    }
}
