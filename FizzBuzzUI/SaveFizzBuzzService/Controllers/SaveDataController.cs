﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SaveFizzBuzzService.Interface;

namespace SaveFizzBuzzService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaveFizzBuzzDataController : ControllerBase, ISaveFizzBuzzData
    {

        [HttpPost]
        public bool SaveDataDB(int enteredData)
        {
            SaveDataModel saveData = new SaveDataModel
            {
                EnteredData = enteredData,
                EnteredDateTime = DateTime.Now
            };
            return true;
        }
    }
}
