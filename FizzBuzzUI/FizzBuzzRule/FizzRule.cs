﻿using System;
using DayOfTheWeek.Interface;
using DayOfTheWeek.Service;
using FizzBuzzRuleAll.Constatnt;

namespace FizzBuzzRuleAll
{
    public class FizzRule : IFizzBuzzRule
    {
        DayOfTheWeekCase _dayOfTheWeek;
        private readonly string isDayWeek;
        //private readonly IDayOfTheWeek _dayOfTheWeek;
        public FizzRule()
        {
            _dayOfTheWeek = new DayOfTheWeekCase();
            isDayWeek = ((IDayOfTheWeek)_dayOfTheWeek).CurrentDayOfTheWeek();
            //_dayOfTheWeek = dayOfTheWeek;
        }
        public bool NumberMatch(int enteredNum)
        {
            return enteredNum % 3 == 0;
        }

        public string GetReplacedWord()
        {
            if (isDayWeek == FizzBuzzConstant.DayWeekRule)
            {
                return "Wizz";
            }
            else
            {
                return "Fizz";
            }            
        }
    }

}
