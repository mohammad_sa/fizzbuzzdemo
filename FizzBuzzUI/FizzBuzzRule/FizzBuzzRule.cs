﻿using System;
using DayOfTheWeek.Interface;
using DayOfTheWeek.Service;
using FizzBuzzRuleAll.Constatnt;

namespace FizzBuzzRuleAll
{
    public class FizzBuzzRule : IFizzBuzzRule
    {
        DayOfTheWeekCase _dayOfTheWeek;
        private readonly string dayWeek;
        //private readonly IDayOfTheWeek _dayOfTheWeek;
        public FizzBuzzRule()
        {
            _dayOfTheWeek = new DayOfTheWeekCase();
            dayWeek = ((IDayOfTheWeek)_dayOfTheWeek).CurrentDayOfTheWeek();
            //_dayOfTheWeek = dayOfTheWeek;
        }
        public bool NumberMatch(int enteredNum)
        {
            return enteredNum % 3 == 0 && enteredNum % 5 == 0;
        }

        public string GetReplacedWord()
        {
            if (dayWeek == FizzBuzzConstant.DayWeekRule)
            {
                return "WizzWuzz";
            }
            else
            {
                return "FizzBuzz";
            }
        }
    }
}
