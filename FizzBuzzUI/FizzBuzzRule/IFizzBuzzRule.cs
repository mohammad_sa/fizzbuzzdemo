﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzzRuleAll
{
    public interface IFizzBuzzRule
    {
        bool NumberMatch(int enteredNum);
        string GetReplacedWord();
    }
}
